const assert = require('assert');
const chai = require('chai'), spies = require('chai-spies');

chai.use(spies);

describe('Test "corex.example" intent', () => {
    let evHandler = () => {};

    it('example test', done => {
        let intent = 'corex.example';

        let response = testTools.getIaResponse('this is an example', {
            a: 'a',
            b: 'b',
            c: 'c'
        }, intent);

        evHandler = data => {
            let reply = data.response;

            // Asserts
            assert.equal(reply.type, 'simple');

            done();
        };

        eventEmitter.on('bot-reply', evHandler);

        testTools.fireIntentAction('astibotModule', intent, response, null);
    });

    afterEach(function() {
        eventEmitter.removeListener('bot-reply', evHandler);
    });
});