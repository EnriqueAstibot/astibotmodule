logger.info('Corex: modulo ' + module.filename);

const UserStorage = require('manageUserStorage');
const { response } = require('express');

const request = require('request');
const { Logger } = require('winston');

let municipios = null;
let noticias = null;
let ofertas = null;
let ofertasAll = [];

const routesNum = (Number.isInteger(Number(process.env.routesNum))) ? process.env.routesNum : 3;

const isGoingToRain = (message, resp, bot) => {

    let municipio = (Array.isArray(resp.result.parameters['poblacion']) ? resp.result.parameters['poblacion'][0] : resp.result.parameters['poblacion']);

    if (municipio) {
        var options = {
            'method': 'POST',
            // 'url': 'https://api.openweathermap.org/data/2.5/weather?q=' + municipio + ',es&units=metric&appid=e12f75ffa22d714982ac328b568e9d99',
            'url': 'http://api.weatherapi.com/v1/forecast.json?key=2aebfc71d0244c8da77125450202610&q=' + municipio + '&days=1',
            'headers': {
            }
        };
        request(options, function (error, response) {
            if (error) throw new Error(error)
            let res;
            if (JSON.parse(response.body).error) res = "Lo siento, no sabría decirte.";
            else {
                if(JSON.parse(response.body).forecast.forecastday[0].day.daily_will_it_rain == 1){
                    res += '<p>Parece que va a llover, la probabilidad de lluvia de hoy es de un '+JSON.parse(response.body).forecast.forecastday[0].day.daily_chance_of_rain+'% </p>';
                }else{
                    res += '<p>Parece que no va a llover, la probabilidad de lluvia de hoy es de un '+JSON.parse(response.body).forecast.forecastday[0].day.daily_chance_of_rain+'% </p>';
                }
            }
            utils.reply(bot, message, {
                text: res,
                type: 'simple'
            });
        });
    } else {
        utils.reply(bot, message, {
            text: 'Lo siento, pero no se si va a llover.',
            type: 'simple'
        });
    }

}

const getTiempo = (message, resp, bot) => {

    let municipio = (Array.isArray(resp.result.parameters['poblacion']) ? resp.result.parameters['poblacion'][0] : resp.result.parameters['poblacion']);

    if (municipio) {
        var options = {
            'method': 'POST',
            // 'url': 'https://api.openweathermap.org/data/2.5/weather?q=' + municipio + ',es&units=metric&appid=e12f75ffa22d714982ac328b568e9d99',
            'url': 'http://api.weatherapi.com/v1/forecast.json?key=2aebfc71d0244c8da77125450202610&q=' + municipio + '&days=1',
            'headers': {
            }
        };
        request(options, function (error, response) {
            if (error) throw new Error(error)
            let res;
            if (JSON.parse(response.body).error) res = "No hay datos climatológicos para la población seleccionada.";
            else {
                res = '<p><b>Temperatura actual:</b></p><p>' + JSON.parse(response.body).current.temp_c + ' grados</p>';
                res += '<p><b>Predicción de hoy:</b></p><p>Máxima: '+JSON.parse(response.body).forecast.forecastday[0].day.maxtemp_c+'</p><p>Mínima: '+JSON.parse(response.body).forecast.forecastday[0].day.mintemp_c+'</p><p>Probabilidad de lluvia: '+JSON.parse(response.body).forecast.forecastday[0].day.daily_chance_of_rain+'%</p>'
            }
            utils.reply(bot, message, {
                text: res,
                type: 'simple'
            });
        });
    } else {
        utils.reply(bot, message, {
            text: 'Lo siento, pero no hay datos para el municipio introducido o no pertenece a la provincia.',
            type: 'simple'
        });
    }

}

const getHora = (message, resp, bot) => {
    let date = new Date();
    if (date.getHours() == 1) {
        utils.reply(bot, message, {
            text: 'Es la ' + date.toLocaleTimeString('es-ES'),
            type: 'simple'
        });
    } else {
        utils.reply(bot, message, {
            text: 'Son las ' + date.toLocaleTimeString('es-ES'),
            type: 'simple'
        });
    }
}

const getFecha = (message, resp, bot) => {
    let date = new Date();
    const options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
    utils.reply(bot, message, {
        text: 'Hoy es ' + date.toLocaleDateString('es-ES'),
        type: 'simple'
    });
}

const getAllMunicipios = () => {
    return new Promise(function (resolve, reject) {
        if (municipios) return resolve();

        request.get({ url: 'https://turismoapps.badajoz.es/api/municipios', strictSSL: false }, function (error, response, body) {
            if (error) return reject(error);

            if (_isValidJSON(body)) {
                municipios = JSON.parse(body);
            }
            resolve();
        });
    });
};

const getMunicipio = (nameMunicipality) => {
    return getAllMunicipios().then(() => {
        return new Promise(function (resolve, reject) {
            const mun = (municipios) ? municipios.find(obj => obj.title === nameMunicipality) : null;
            if (!mun) return resolve();
            request.get({ url: 'https://turismoapps.badajoz.es/api/municipio?nid=' + mun.nid, strictSSL: false }, function (error, response, body) {
                if (error) return reject(error);
                let data = null;
                if (_isValidJSON(body)) {
                    data = JSON.parse(body);
                }
                resolve(data);
            });
        });
    });
};

const getCoordenadas = (message, resp, bot) => {

    let municipio = (Array.isArray(resp.result.parameters['poblacion']) ? resp.result.parameters['poblacion'][0] : resp.result.parameters['poblacion']);

    getMunicipio(municipio).then((data) => {
        if (data && data.field_ubicacion_lat && data.field_ubicacion_lon) {
            utils.reply(bot, message, {
                text: "<iframe src='https://maps.google.com/?ll=" + data.field_ubicacion_lat + "," + data.field_ubicacion_lon + "&z=14&output=embed' frameborder=0></iframe>",
                type: 'simple'
            });
        } else {
            utils.reply(bot, message, {
                text: "No se ha podido geolocalizar la población indicada",
                type: 'simple'
            });
        }
    }).catch(err => {
        logger.error(err)
    });

}

const getMonumentosMunicipio = (nameMunicipality, userStorage) => {
    return getAllMunicipios().then(() => {
        return new Promise(function (resolve, reject) {
            const mun = (municipios) ? municipios.find(obj => obj.title === nameMunicipality) : null;
            if (!mun) return resolve();
            request.get({ url: 'http://turismoapps.badajoz.es/api/poi?id_municipio=' + mun.nid, strictSSL: false }, function (error, response, body) {
                if (error) return reject(error);
                let data = null;
                if (_isValidJSON(body)) {
                    data = JSON.parse(body);
                }
                userStorage.set('AstibotModule', data);
                userStorage.remove('AstibotModule_intent');
                userStorage.set('AstibotModule_intent', 'getMonumentosMunicipio');
                resolve(data);
            });
        });
    });
};

const getMonumento = (message, resp, bot) => {

    let municipio = (Array.isArray(resp.result.parameters['poblacion']) ? resp.result.parameters['poblacion'][0] : resp.result.parameters['poblacion']);

    const userStorage = new UserStorage(message.channel, {persist:true});
    
    if(municipio) {
        getMonumentosMunicipio(municipio, userStorage).then((data) => {

            if (data) {
                let responses = ['¡Aquí tienes!'];

                let firstIndex = 0;
                let lastIndex = routesNum;

                for (let i = firstIndex; i < lastIndex; i++) {
                    let item = data[i];
                    try{
                        if (!item.title) {
                            lastIndex++;
                            continue;
                        }
                        if (!item.descripcion) {
                            lastIndex++;
                            continue;
                        }
                        if (!item.field_imagen_principal_path) {
                            lastIndex++;
                            continue;
                        }
                    } catch(e){
                        continue;
                    }
                    item.field_imagen_principal_path = item.field_imagen_principal_path.split(' ').join('%20');
                    responses.push('<p><b>' + item.title + '.</b></p><p>' + item.descripcion + '</p>' + item.field_imagen_principal_path)
                }
                userStorage.set('AstibotModule_i', lastIndex);
                if(data.length > lastIndex) {
                    responses.push(`He encontrado ${data.length-lastIndex} resultados más. Si quieres puedo mostrarte más.`);
                } 
                if (responses.length > 1) {
                    utils.reply(bot, message, {
                        text: responses,
                        type: 'multiple'
                    });
                } else {
                    userStorage.remove('AstibotModule');
                    userStorage.remove('AstibotModule_intent');
                    utils.reply(bot, message, {
                        text: "No hay datos de monumentos para la población indicada",
                        type: 'simple'
                    });
                }
            } else {
                utils.reply(bot, message, {
                    text: "No hay datos de monumentos para la población indicada",
                    type: 'simple'
                });
            }
        }).catch(err => {
            logger.error(err)
        });
    } else if(userStorage.get('AstibotModule_intent') == 'getMonumentosMunicipio' && message.intent == 'info.monumento.more') {
        let data = userStorage.get('AstibotModule');
        let firstIndex = userStorage.get('AstibotModule_i');
        let lastIndex = firstIndex + routesNum;
        
        if (data) {
            let responses = ['¡Aquí tienes!'];

            for (let i = firstIndex; i < lastIndex; i++) {
                let item = data[i];
                try{
                    if (!item.title) {
                        lastIndex++;
                        continue;
                    }
                    if (!item.descripcion) {
                        lastIndex++;
                        continue;
                    }
                    if (!item.field_imagen_principal_path) {
                        lastIndex++;
                        continue;
                    }
                } catch(e){
                    continue;
                }
                item.field_imagen_principal_path = item.field_imagen_principal_path.split(' ').join('%20');
                responses.push('<p><b>' + item.title + '.</b></p><p>' + item.descripcion + '</p>' + item.field_imagen_principal_path)
            }
            userStorage.set('AstibotModule_i', lastIndex);
            if(data.length > lastIndex) {
                responses.push(`He encontrado ${data.length-lastIndex} resultados más. Si quieres puedo mostrarte más.`);
            }
            if (responses.length > 1) {
                utils.reply(bot, message, {
                    text: responses,
                    type: 'multiple'
                });
            } else {
                userStorage.remove('AstibotModule');
                userStorage.remove('AstibotModule_intent');
                utils.reply(bot, message, {
                    text: "No hay datos de monumentos para la población indicada",
                    type: 'simple'
                });
            }
        } else {
            utils.reply(bot, message, {
                text: "No hay datos de monumentos para la población indicada",
                type: 'simple'
            });
        }

    } else {
        utils.reply(bot, message, {
            text: "Lo siento, pero para poder mostrarte puntos de interés necesito saber el municipio del que desea la información. Por favor, añada el municipio del que desea la información a su consulta.",
            type: 'multiple'
        });
    }
}


const getAllEmpresas = () => {
    return new Promise(function (resolve, reject) {
        request.get({ url: 'https://turismoapps.badajoz.es/api/activos?tipo=empresa', strictSSL: false }, function (error, response, body) {
            if (error) return reject(error);
            if (!_isValidJSON(body)) return reject(error);
            resolve(JSON.parse(body));
        });
    });
};

const getEmpresasByNid = (userStorage) => {
    return new Promise(function (resolve, reject) {
        getAllEmpresas().then(async (result) => {
            let empresas = [];
            let i = 0;
            for (let item of result) {
                let empresa = await getEmpresaByNid(item.nid);
                if (empresa != null) empresas.push(empresa);
                i++;
                if(i == 10) break;
            }
            userStorage.set('AstibotModule', empresas);
            userStorage.remove('AstibotModule_intent');
            userStorage.set('AstibotModule_intent', 'getEmpresasByNid');
            resolve(empresas);
        });
    });
};

const getEmpresaByNid = async (nid) => {
    return new Promise(function (resolve, reject) {
        request.get({ url: 'http://turismoapps.badajoz.es/api/empresa?nid=' + nid, strictSSL: false }, function (error, response, body) {
            if (error) {
                resolve(null);
                return;
            }
            if (!_isValidJSON(body)) {
                resolve(null);
                return;
            }
            let data = JSON.parse(body);
            resolve(data[0]);
        });
    });
};

const getEmpresas = (message, resp, bot) => {

    const userStorage = new UserStorage(message.channel, {persist:true});
    
    if(userStorage.get('AstibotModule_intent') == 'getEmpresasByNid'){
        let data = userStorage.get('AstibotModule');
        let firstIndex = userStorage.get('AstibotModule_i');
        let lastIndex = firstIndex + routesNum;
        
        if (data) {
            let responses = ['¡Aquí tienes!'];

            for (let i = firstIndex; i < lastIndex; i++) {
                let item = data[i];
                try{
                    if (!item.title) {
                        lastIndex++;
                        continue;
                    }
                    if (!item.field_imagen_principal_path) {
                        lastIndex++;
                        continue;
                    }
                } catch(e){
                    continue;
                }              
                text = '';
                text += '<p><b>' + item.title + '.</b></p>';
                if (item.sector_negocio) text += '<p>Sector: ' + item.sector_negocio + '.</p>';
                if (item.telefono) text += '<p>Teléfono: ' + item.telefono + '.</p>';
                if (item.correo_electronico) text += '<p>Correo: ' + item.correo_electronico + '.</p>';
                if (item.web) {
                    if(item.web.substr(item.web.length -1) == '/') item.web = item.web.slice(0,-1);
                    text += '<p>Web: ' + item.web + '.</p>';
                }
                item.field_imagen_principal_path = item.field_imagen_principal_path.split(' ').join('%20');
                if (item.field_imagen_principal_path) text += item.field_imagen_principal_path;
                responses.push(text);
            }
            userStorage.set('AstibotModule_i', lastIndex);
            if(data.length > lastIndex) {
                responses.push(`He encontrado ${data.length-lastIndex} resultados más. Si quieres puedo mostrarte más.`);
            } 
            if (responses.length > 1) {
                utils.reply(bot, message, {
                    text: responses,
                    type: 'multiple'
                });
            } else {
                userStorage.remove('AstibotModule');
                userStorage.remove('AstibotModule_intent');
                utils.reply(bot, message, {
                    text: "No hay datos de empresas",
                    type: 'simple'
                });
            }
        } else {
            utils.reply(bot, message, {
                text: "No hay datos de empresas",
                type: 'simple'
            });
        }
    } else{
        getEmpresasByNid(userStorage).then((data) => {

            if (data) {
                let responses = ['¡Aquí tienes!'];

                let firstIndex = 0;
                let lastIndex = routesNum;

                for (let i = firstIndex; i < lastIndex; i++) {
                    let item = data[i];
                    try{
                        if (!item.title) {
                            lastIndex++;
                            continue;
                        }
                        if (!item.field_imagen_principal_path) {
                            lastIndex++;
                            continue;
                        }
                    } catch(e){
                        continue;
                    }
                    text = '';
                    text += '<p><b>' + item.title + '.</b></p>';
                    if (item.sector_negocio) text += '<p>Sector: ' + item.sector_negocio + '.</p>';
                    if (item.telefono) text += '<p>Teléfono: ' + item.telefono + '.</p>';
                    if (item.correo_electronico) text += '<p>Correo: ' + item.correo_electronico + '.</p>';
                    if (item.web) text += '<p>Web: ' + item.web + '.</p>';
                    item.field_imagen_principal_path = item.field_imagen_principal_path.split(' ').join('%20');
                    if (item.field_imagen_principal_path) text += item.field_imagen_principal_path;
                    responses.push(text);
                }
                userStorage.set('AstibotModule_i', lastIndex);
                if(data.length > lastIndex) {
                    responses.push(`He encontrado ${data.length-lastIndex} resultados más. Si quieres puedo mostrarte más.`);
                } 
                if (responses.length > 1) {
                    utils.reply(bot, message, {
                        text: responses,
                        type: 'multiple'
                    });
                } else {
                    userStorage.remove('AstibotModule');
                    userStorage.remove('AstibotModule_intent');
                    utils.reply(bot, message, {
                        text: "No hay datos de empresas",
                        type: 'simple'
                    });
                }
            } else {
                utils.reply(bot, message, {
                    text: "No hay datos de empresas",
                    type: 'simple'
                });
            }
        }).catch(err => {
            logger.error(err)
        });
    }

}

const getEmpresasFromMunicipio = (nameMunicipality, userStorage) => {
    return getAllMunicipios().then(() => {
        return new Promise(function (resolve, reject) {
            const mun = (municipios) ? municipios.find(obj => obj.title === nameMunicipality) : null;
            if (!mun) return resolve();
            request.get({ url: 'http://turismoapps.badajoz.es/api/empresa?id_municipio=' + mun.nid, strictSSL: false }, function (error, response, body) {
                if (error) return reject(error);
                let data = null;
                if (_isValidJSON(body)) {
                    data = JSON.parse(body);
                }
                userStorage.set('AstibotModule', data);
                userStorage.remove('AstibotModule_intent');
                userStorage.set('AstibotModule_intent', 'getEmpresasFromMunicipio');
                resolve(data);
            });
        });
    });
};

const getEmpresasMunicipio = (message, resp, bot) => {

    let municipio = (Array.isArray(resp.result.parameters['poblacion']) ? resp.result.parameters['poblacion'][0] : resp.result.parameters['poblacion']);

    const userStorage = new UserStorage(message.channel, {persist:true});

    if(municipio) {
        getEmpresasFromMunicipio(municipio, userStorage).then((data) => {

            if (data) {
                let responses = ['¡Aquí tienes!'];

                let firstIndex = 0;
                let lastIndex = routesNum;

                for (let i = firstIndex; i < lastIndex; i++) {
                    let item = data[i];
                    try{
                        if (!item.title) {
                            lastIndex++;
                            continue;
                        }
                        if (!item.field_imagen_principal_path) {
                            lastIndex++;
                            continue;
                        }
                    } catch(e){
                        continue;
                    }
                    text = '';
                    text += '<p><b>' + item.title + '.</b></p>';
                    if (item.sector_negocio) text += '<p>Sector: ' + item.sector_negocio + '.</p>';
                    if (item.telefono) text += '<p>Teléfono: ' + item.telefono + '.</p>';
                    if (item.correo_electronico) text += '<p>Correo: ' + item.correo_electronico + '.</p>';
                    if (item.web) {
                        if(item.web.charAt(item.web.length - 1) == '/'){
                             text += '<p>Web: ' + item.web.slice(0, -1) + '.</p>';
                        } else {
                            text += '<p>Web: ' + item.web + '.</p>';
                        }
                    }
                    item.field_imagen_principal_path = item.field_imagen_principal_path.split(' ').join('%20');
                    if (item.field_imagen_principal_path) text += item.field_imagen_principal_path;
                    responses.push(text);
                }
                userStorage.set('AstibotModule_i', lastIndex);
                if(data.length > lastIndex) {
                    responses.push(`He encontrado ${data.length-lastIndex} resultados más. Si quieres puedo mostrarte más.`);
                } 
                if (responses.length > 1) {
                    utils.reply(bot, message, {
                        text: responses,
                        type: 'multiple'
                    });
                } else {
                    userStorage.remove('AstibotModule');
                    userStorage.remove('AstibotModule_intent');
                    utils.reply(bot, message, {
                        text: "No hay datos de empresas para la población indicada",
                        type: 'simple'
                    });
                }
            } else {
                utils.reply(bot, message, {
                    text: "No hay datos de empresas para la población indicada",
                    type: 'simple'
                });
            }
        }).catch(err => {
            logger.error(err)
        });
    } else {
        let data = userStorage.get('AstibotModule');
        let firstIndex = userStorage.get('AstibotModule_i');
        let lastIndex = firstIndex + routesNum;
        
        if (data) {
            let responses = ['¡Aquí tienes!'];

            for (let i = firstIndex; i < lastIndex; i++) {
                let item = data[i];
                try{
                    if (!item.title) {
                        lastIndex++;
                        continue;
                    }
                    if (!item.field_imagen_principal_path) {
                        lastIndex++;
                        continue;
                    }
                } catch(e){
                    continue;
                }
                text = '';
                text += '<p><b>' + item.title + '.</b></p>';
                if (item.sector_negocio) text += '<p>Sector: ' + item.sector_negocio + '.</p>';
                if (item.telefono) text += '<p>Teléfono: ' + item.telefono + '.</p>';
                if (item.correo_electronico) text += '<p>Correo: ' + item.correo_electronico + '.</p>';
                if (item.web) text += '<p>Web: ' + item.web + '.</p>';
                item.field_imagen_principal_path = item.field_imagen_principal_path.split(' ').join('%20');
                if (item.field_imagen_principal_path) text += item.field_imagen_principal_path;
                responses.push(text);
            }
            userStorage.set('AstibotModule_i', lastIndex);
            if(data.length > lastIndex) {
                responses.push(`He encontrado ${data.length-lastIndex} resultados más. Si quieres puedo mostrarte más.`);
            } 
            if (responses.length > 1) {
                utils.reply(bot, message, {
                    text: responses,
                    type: 'multiple'
                });
            } else {
                userStorage.remove('AstibotModule');
                userStorage.remove('AstibotModule_intent');
                utils.reply(bot, message, {
                    text: "No hay datos de empresas para la población indicada",
                    type: 'simple'
                });
            }
        } else {
            utils.reply(bot, message, {
                text: "No hay datos de empresas para la población indicada",
                type: 'simple'
            });
        }

    }
}

const getOfertasFromMunicipio = (nameMunicipality, userStorage) => {
    return getAllMunicipios().then(() => {
        return new Promise(function (resolve, reject) {
            const mun = (municipios) ? municipios.find(obj => obj.title === nameMunicipality) : null;
            if (!mun) return resolve();
            request.get({ url: 'http://turismoapps.badajoz.es/api/oferta?id_municipio=' + mun.nid, strictSSL: false }, function (error, response, body) {
                if (error) return reject(error);
                let data = null;
                if (_isValidJSON(body)) {
                    data = JSON.parse(body);
                }
                userStorage.set('AstibotModule', data);
                userStorage.remove('AstibotModule_intent');
                userStorage.set('AstibotModule_intent', 'getOfertasFromMunicipio');
                resolve(data);
            });
        });
    });
};

const getOfertas = (message, resp, bot) => {

    const userStorage = new UserStorage(message.channel, {persist:true});
    
    if(userStorage.get('AstibotModule_intent') == 'getOfertasByNid'){
        let data = userStorage.get('AstibotModule');
        let firstIndex = userStorage.get('AstibotModule_i');
        let lastIndex = firstIndex + routesNum;
        
        if (data) {
            let responses = ['¡Aquí tienes!'];

            for (let i = firstIndex; i < lastIndex; i++) {
                let item = data[i];
                try{
                    if (!item.title) {
                        lastIndex++;
                        continue;
                    }
                } catch(e){
                    continue;
                }
                text = '';
                text += '<p><b>' + item.title + '.</b></p>';
                if (item.descripcion) text += '<p>' + item.descripcion + '.</p>';
                if (item.fecha_fin) text += '<p>Validez: ' + item.fecha_inicio + ' - ' + item.fecha_fin + '.</p>';
                if (item.field_imagen_principal_path){
                    item.field_imagen_principal_path = item.field_imagen_principal_path.split(' ').join('%20');
                    text += item.field_imagen_principal_path;
                }
                console.log(text)
                responses.push(text);
            }
            userStorage.set('AstibotModule_i', lastIndex);
            if(data.length > lastIndex) {
                responses.push(`He encontrado ${data.length-lastIndex} resultados más. Si quieres puedo mostrarte más.`);
            }  
            if (responses.length > 1) {
                utils.reply(bot, message, {
                    text: responses,
                    type: 'multiple'
                });
            } else {
                userStorage.remove('AstibotModule');
                userStorage.remove('AstibotModule_intent');
                utils.reply(bot, message, {
                    text: "No se han encontrado ofertas",
                    type: 'simple'
                });
            }
        } else {
            utils.reply(bot, message, {
                text: "No se han encontrado ofertas",
                type: 'simple'
            });
        }
    } else {
        getOfertasByNid(userStorage).then((data) => {
            
            if (data) {
                let responses = ['¡Aquí tienes!'];

                let firstIndex = 0;
                let lastIndex = routesNum;

    
                for (let i = firstIndex; i < lastIndex; i++) {
                    let item = data[i];
                    try{
                        if (!item.title) {
                            lastIndex++;
                            continue;
                        }
                    } catch(e){
                        continue;
                    }
                    text = '';
                    text += '<p><b>' + item.title + '.</b></p>';
                    if (item.descripcion) text += '<p>' + item.descripcion + '.</p>';
                    if (item.fecha_fin) text += '<p>Validez: ' + item.fecha_inicio + ' - ' + item.fecha_fin + '.</p>';
                    if (item.field_imagen_principal_path){
                        item.field_imagen_principal_path = item.field_imagen_principal_path.split(' ').join('%20');
                        text += item.field_imagen_principal_path;
                    }
                    responses.push(text);
                }
                userStorage.set('AstibotModule_i', lastIndex);
                if(data.length > lastIndex) {
                    responses.push(`He encontrado ${data.length-lastIndex} resultados más. Si quieres puedo mostrarte más.`);
                } 
                if (responses.length > 1) {
                    utils.reply(bot, message, {
                        text: responses,
                        type: 'multiple'
                    });
                } else {
                    userStorage.remove('AstibotModule');
                    userStorage.remove('AstibotModule_intent');
                    utils.reply(bot, message, {
                        text: "No se han encontrado ofertas",
                        type: 'simple'
                    });
                }
            } else {
                utils.reply(bot, message, {
                    text: "No se han encontrado ofertas",
                    type: 'simple'
                });
            }
        }).catch(err => {
            logger.error(err)
        });
    }
}

const getAllOfertas = () => {
    return new Promise(function (resolve, reject) {
        request.get({ url: 'https://turismoapps.badajoz.es/api/activos?tipo=oferta', strictSSL: false }, function (error, response, body) {
            if (error) return reject(error);
            if (!_isValidJSON(body)) return reject(error);
            resolve(JSON.parse(body));
        });
    });
};

const getOfertasByNid = (userStorage) => {
    return new Promise(function (resolve, reject) {
        getAllOfertas().then(async (result) => {
            let ofertas = [];
            for (let item of result) {
                let oferta = await getOfertaByNid(item.nid);
                if (oferta != null) ofertas.push(oferta);
            }
            userStorage.set('AstibotModule', ofertas);
            userStorage.remove('AstibotModule_intent');
            userStorage.set('AstibotModule_intent', 'getOfertasByNid');
            resolve(ofertas);
        });
    });
};

const getOfertaByNid = async (nid) => {
    return new Promise(function (resolve, reject) {
        request.get({ url: 'http://turismoapps.badajoz.es/api/oferta?nid=' + nid, strictSSL: false }, function (error, response, body) {
            if (error) {
                resolve(null);
                return;
            }
            if (!_isValidJSON(body)) {
                resolve(null);
                return;
            }
            let data = JSON.parse(body);
            resolve(data[0]);
        });
    });
};

const getOfertasMunicipio = (message, resp, bot) => {

    let municipio = (Array.isArray(resp.result.parameters['poblacion']) ? resp.result.parameters['poblacion'][0] : resp.result.parameters['poblacion']);

    const userStorage = new UserStorage(message.channel, {persist:true});

    if(municipio) {
        getOfertasFromMunicipio(municipio, userStorage).then((data) => {

            if (data) {
                let responses = ['¡Aquí tienes!'];

                let firstIndex = 0;
                let lastIndex = routesNum;

                for (let i = firstIndex; i < lastIndex; i++) {
                    let item = data[i];
                    try{
                        if (!item.title) {
                            lastIndex++;
                            continue;
                        }
                    } catch(e){
                        continue;
                    }
                    text = '';
                    text += '<p><b>' + item.title + '.</b></p>';
                    if (item.descripcion) text += '<p>' + item.descripcion + '.</p>';
                    if (item.fecha_fin) text += '<p>Validez: ' + item.fecha_inicio + ' - ' + item.fecha_fin + '.</p>';
                    if (item.field_imagen_principal_path){
                        item.field_imagen_principal_path = item.field_imagen_principal_path.split(' ').join('%20');
                        text += item.field_imagen_principal_path;
                    }
                    responses.push(text);
                }
                userStorage.set('AstibotModule_i', lastIndex);
                if(data.length > lastIndex) {
                    responses.push(`He encontrado ${data.length-lastIndex} resultados más. Si quieres puedo mostrarte más.`);
                } 
                if (responses.length > 1) {
                    utils.reply(bot, message, {
                        text: responses,
                        type: 'multiple'
                    });
                } else {
                    userStorage.remove('AstibotModule');
                    userStorage.remove('AstibotModule_intent');
                    utils.reply(bot, message, {
                        text: "No hay ofertas para la población indicada",
                        type: 'simple'
                    });
                }
            } else {
                utils.reply(bot, message, {
                    text: "No se han encontrado ofertas para la población indicada",
                    type: 'simple'
                });
            }
        }).catch(err => {
            logger.error(err)
        });
    }else {
        let data = userStorage.get('AstibotModule');
        let firstIndex = userStorage.get('AstibotModule_i');
        let lastIndex = firstIndex + routesNum;
        
        if (data) {
            let responses = ['¡Aquí tienes!'];

            for (let i = firstIndex; i < lastIndex; i++) {
                let item = data[i];
                try{
                    if (!item.title) {
                        lastIndex++;
                        continue;
                    }
                } catch(e){
                    continue;
                }
                text = '';
                text += '<p><b>' + item.title + '.</b></p>';
                if (item.descripcion) text += '<p>' + item.descripcion + '.</p>';
                if (item.fecha_fin) text += '<p>Validez: ' + item.fecha_inicio + ' - ' + item.fecha_fin + '.</p>';
                if (item.field_imagen_principal_path){
                    item.field_imagen_principal_path = item.field_imagen_principal_path.split(' ').join('%20');
                    text += item.field_imagen_principal_path;
                }
                responses.push(text);
            }
            userStorage.set('AstibotModule_i', lastIndex);
            if(data.length > lastIndex) {
                responses.push(`He encontrado ${data.length-lastIndex} resultados más. Si quieres puedo mostrarte más.`);
            } 
            if (responses.length > 1) {
                utils.reply(bot, message, {
                    text: responses,
                    type: 'multiple'
                });
            } else {
                userStorage.remove('AstibotModule');
                userStorage.remove('AstibotModule_intent');
                utils.reply(bot, message, {
                    text: "No hay ofertas para la población indicada",
                    type: 'simple'
                });
            }
        } else {
            utils.reply(bot, message, {
                text: "No se han encontrado ofertas para la población indicada",
                type: 'simple'
            });
        }
    }
}

const getAllEventos = () => {
    return new Promise(function (resolve, reject) {
        request.get({ url: 'https://turismoapps.badajoz.es/api/activos?tipo=evento', strictSSL: false }, function (error, response, body) {
            if (error) return reject(error);
            if (!_isValidJSON(body)) return reject(error);
            resolve(JSON.parse(body));
        });
    });
};

const getEventosFromMunicipio = (nameMunicipality, userStorage) => {
    return getAllMunicipios().then(() => {
        return new Promise(function (resolve, reject) {
            const mun = (municipios) ? municipios.find(obj => obj.title === nameMunicipality) : null;
            if (!mun) return resolve();
            request.get({ url: 'http://turismoapps.badajoz.es/api/evento?id_municipio=' + mun.nid, strictSSL: false }, function (error, response, body) {
                if (error) return reject(error);
                let data = null;
                if (_isValidJSON(body)) {
                    data = JSON.parse(body);
                }
                userStorage.set('AstibotModule', data);
                userStorage.remove('AstibotModule_intent');
                userStorage.set('AstibotModule_intent', 'getEventosFromMunicipio');
                resolve(data);
            });
        });
    });
};

const getEventosMunicipio = (message, resp, bot) => {

    let municipio = (Array.isArray(resp.result.parameters['poblacion']) ? resp.result.parameters['poblacion'][0] : resp.result.parameters['poblacion']);

    const userStorage = new UserStorage(message.channel, {persist:true});

    if(municipio) {
        getEventosFromMunicipio(municipio, userStorage).then((data) => {

            if (data) {
                let responses = ['¡Aquí tienes!'];

                let firstIndex = 0;
                let lastIndex = routesNum;

                for (let i = firstIndex; i < lastIndex; i++) {
                    let item = data[i];
                    try{
                        if (!item.title) {
                            lastIndex++;
                            continue;
                        }
                    } catch(e){
                        continue;
                    }
                    text = '';
                    text += '<p><b>' + item.title + '.</b></p>';
                    if (item.descripcion) text += '<p>' + item.descripcion + '.</p>';
                    if (item.fecha_fin) text += '<p>Validez: ' + item.fecha_hora_inicio + ' - ' + item.fecha_hora_fin + '.</p>';
                    if (item.field_imagen_principal_path){
                        item.field_imagen_principal_path = item.field_imagen_principal_path.split(' ').join('%20');
                        text += item.field_imagen_principal_path;
                    }
                    responses.push(text);
                }
                userStorage.set('AstibotModule_i', lastIndex);
                if(data.length > lastIndex) {
                    responses.push(`He encontrado ${data.length-lastIndex} resultados más. Si quieres puedo mostrarte más.`);
                } 
                if (responses.length > 1) {
                    utils.reply(bot, message, {
                        text: responses,
                        type: 'multiple'
                    });
                } else {
                    userStorage.remove('AstibotModule');
                    userStorage.remove('AstibotModule_intent');
                    utils.reply(bot, message, {
                        text: "No se han encontrado eventos para la población indicada",
                        type: 'simple'
                    });
                }
            } else {
                utils.reply(bot, message, {
                    text: "No se han encontrado eventos para la población indicada",
                    type: 'simple'
                });
            }
        }).catch(err => {
            logger.error(err)
        });
    }else {
        let data = userStorage.get('AstibotModule');
        let firstIndex = userStorage.get('AstibotModule_i');
        let lastIndex = firstIndex + routesNum;
        
        if (data) {
            let responses = ['¡Aquí tienes!'];

            for (let i = firstIndex; i < lastIndex; i++) {
                let item = data[i];
                try{
                    if (!item.title) {
                        lastIndex++;
                        continue;
                    }
                } catch(e){
                    continue;
                }
                text = '';
                text += '<p><b>' + item.title + '.</b></p>';
                if (item.descripcion) text += '<p>' + item.descripcion + '.</p>';
                if (item.fecha_fin) text += '<p>Validez: ' + item.fecha_hora_inicio + ' - ' + item.fecha_hora_fin + '.</p>';
                if (item.field_imagen_principal_path){
                    item.field_imagen_principal_path = item.field_imagen_principal_path.split(' ').join('%20');
                    text += item.field_imagen_principal_path;
                } 

                responses.push(text);
            }
            userStorage.set('AstibotModule_i', lastIndex);
            if(data.length > lastIndex) {
                responses.push(`He encontrado ${data.length-lastIndex} resultados más. Si quieres puedo mostrarte más.`);
            } 
            if (responses.length > 1) {
                utils.reply(bot, message, {
                    text: responses,
                    type: 'multiple'
                });
            } else {
                userStorage.remove('AstibotModule');
                userStorage.remove('AstibotModule_intent');
                utils.reply(bot, message, {
                    text: "No se han encontrado eventos para la población indicada",
                    type: 'simple'
                });
            }
        } else {
            utils.reply(bot, message, {
                text: "No se han encontrado eventos para la población indicada",
                type: 'simple'
            });
        }
    }
}

const getEventosByNid = (userStorage) => {
    return new Promise(function (resolve, reject) {
        getAllEventos().then(async (result) => {
            let eventos = [];
            let i = 0;
            for (let item of result) {
                let evento = await getEventoByNid(item.nid);
                if (evento != null) eventos.push(evento);
                if(i == 25){
                    break;
                }
                i++;
            }
            userStorage.set('AstibotModule', eventos);
            userStorage.remove('AstibotModule_intent');
            userStorage.set('AstibotModule_intent', 'getEventosByNid');
            resolve(eventos);
        });
    });
};

const getEventoByNid = async (nid) => {
    return new Promise(function (resolve, reject) {
        request.get({ url: 'http://turismoapps.badajoz.es/api/evento?nid=' + nid, strictSSL: false }, function (error, response, body) {
            if (error) {
                resolve(null);
                return;
            }
            if (!_isValidJSON(body)) {
                resolve(null);
                return;
            }
            let data = JSON.parse(body);
            resolve(data[0]);
        });
    });
};

const getEventos = (message, resp, bot) => {

    const userStorage = new UserStorage(message.channel, {persist:true});
    
    if(userStorage.get('AstibotModule_intent') == 'getEventosByNid'){
        let data = userStorage.get('AstibotModule');
        let firstIndex = userStorage.get('AstibotModule_i');
        let lastIndex = firstIndex + routesNum;
        
        if (data) {
            let responses = ['¡Aquí tienes!'];

            for (let i = firstIndex; i < lastIndex; i++) {
                let item = data[i];
                try{
                    if (!item.title) {
                        lastIndex++;
                        continue;
                    }
                } catch(e){
                    continue;
                }
                text = '';
                text += '<p><b>' + item.title + '.</b></p>';
                if (item.descripcion) text += '<p>' + item.descripcion + '.</p>';
                if (item.fecha_fin) text += '<p>Validez: ' + item.fecha_hora_inicio + ' - ' + item.fecha_hora_fin + '.</p>';
                if (item.field_imagen_principal_path){
                    item.field_imagen_principal_path = item.field_imagen_principal_path.split(' ').join('%20');
                    text += item.field_imagen_principal_path;
                } 
                responses.push(text);
            }
            userStorage.set('AstibotModule_i', lastIndex);
            if(data.length > lastIndex) {
                responses.push(`He encontrado ${data.length-lastIndex} resultados más. Si quieres puedo mostrarte más.`);
            } 
            if (responses.length > 1) {
                utils.reply(bot, message, {
                    text: responses,
                    type: 'multiple'
                });
            } else {
                userStorage.remove('AstibotModule');
                userStorage.remove('AstibotModule_intent');
                utils.reply(bot, message, {
                    text: "No se han encontrado eventos",
                    type: 'simple'
                });
            }
        } else {
            utils.reply(bot, message, {
                text: "No se han encontrado eventos",
                type: 'simple'
            });
        }
    } else {
        getEventosByNid(userStorage).then((data) => {
            
            if (data) {
                let responses = ['¡Aquí tienes!'];

                let firstIndex = 0;
                let lastIndex = routesNum;

    
                for (let i = firstIndex; i < lastIndex; i++) {
                    let item = data[i];
                    try{
                        if (!item.title) {
                            lastIndex++;
                            continue;
                        }
                    } catch(e){
                        continue;
                    }
                    text = '';
                    text += '<p><b>' + item.title + '.</b></p>';
                    if (item.descripcion) text += '<p>' + item.descripcion + '.</p>';
                    if (item.fecha_fin) text += '<p>Validez: ' + item.fecha_hora_inicio + ' - ' + item.fecha_hora_fin + '.</p>';
                    if (item.field_imagen_principal_path){
                        item.field_imagen_principal_path = item.field_imagen_principal_path.split(' ').join('%20');
                        text += item.field_imagen_principal_path;
                    } 
                    responses.push(text);
                }
                userStorage.set('AstibotModule_i', lastIndex);
                if(data.length > lastIndex) {
                    responses.push(`He encontrado ${data.length-lastIndex} resultados más. Si quieres puedo mostrarte más.`);
                } 
                if (responses.length > 1) {
                    utils.reply(bot, message, {
                        text: responses,
                        type: 'multiple'
                    });
                } else {
                    userStorage.remove('AstibotModule');
                    userStorage.remove('AstibotModule_intent');
                    utils.reply(bot, message, {
                        text: "No se han encontrado eventos",
                        type: 'simple'
                    });
                }
            } else {
                utils.reply(bot, message, {
                    text: "No se han encontrado eventos",
                    type: 'simple'
                });
            }
        }).catch(err => {
            logger.error(err)
        });
    }
}


const getRutasMunicipio = (nameMunicipality, userStorage) => {
    return getAllMunicipios().then(() => {
        return new Promise(function (resolve, reject) {
            const mun = (municipios) ? municipios.find(obj => obj.title === nameMunicipality) : null;
            if (!mun) return resolve();
            request.get({ url: 'http://turismoapps.badajoz.es/api/ruta?id_municipio=' + mun.nid, strictSSL: false }, function (error, response, body) {
                if (error) return reject(error);
                let data = null;
                if (_isValidJSON(body)) {
                    data = JSON.parse(body);
                }
                userStorage.set('AstibotModule', data);
                userStorage.remove('AstibotModule_intent');
                userStorage.set('AstibotModule_intent', 'getRutasMunicipio');
                resolve(data);
            });
        });
    });
};

const getRutas = (message, resp, bot) => {

    let municipio = (Array.isArray(resp.result.parameters['poblacion']) ? resp.result.parameters['poblacion'][0] : resp.result.parameters['poblacion']);

    const userStorage = new UserStorage(message.channel, {persist:true});
    
    if(municipio) {
        getRutasMunicipio(municipio, userStorage).then((data) => {

            if (data) {
                let responses = ['¡Aquí tienes!'];

                let firstIndex = 0;
                let lastIndex = routesNum;

                for (let i = firstIndex; i < lastIndex; i++) {
                    let item = data[i];
                    try{
                        if (!item.title) {
                            lastIndex++;
                            continue;
                        }
                        if (!item.field_imagen_principal_path) {
                            lastIndex++;
                            continue;
                        }
                    } catch(e){
                        continue;
                    }
                    item.field_imagen_principal_path = item.field_imagen_principal_path.split(' ').join('%20');
                    if(item.field_kml_path) {
                        responses.push('<p><b>' + item.title + '.</b></p><p>' + item.descripcion + '</p><p>Track de la ruta: ' + item.field_kml_path + '.</p>' + item.field_imagen_principal_path)
                    } else {
                        responses.push('<p><b>' + item.title + '.</b></p><p>' + item.descripcion + '</p>' + item.field_imagen_principal_path)
                    }
                }
                userStorage.set('AstibotModule_i', lastIndex);
                if(data.length > lastIndex) {
                    responses.push(`He encontrado ${data.length-lastIndex} resultados más. Si quieres puedo mostrarte más.`);
                } 
                if (responses.length > 1) {
                    utils.reply(bot, message, {
                        text: responses,
                        type: 'multiple'
                    });
                } else {
                    userStorage.remove('AstibotModule');
                    userStorage.remove('AstibotModule_intent');
                    utils.reply(bot, message, {
                        text: "No hay datos de rutas para la población indicada",
                        type: 'simple'
                    });
                }
            } else {
                utils.reply(bot, message, {
                    text: "No hay datos de rutas para la población indicada",
                    type: 'simple'
                });
            }
        }).catch(err => {
            logger.error(err)
        });
    } else if(userStorage.get('AstibotModule_intent') == 'getRutasMunicipio' && message.intent == 'corex.data.route.more') {
        let data = userStorage.get('AstibotModule');
        let firstIndex = userStorage.get('AstibotModule_i');
        let lastIndex = firstIndex + routesNum;
        
        if (data) {
            let responses = ['¡Aquí tienes!'];

            for (let i = firstIndex; i < lastIndex; i++) {
                let item = data[i];
                try{
                    if (!item.title) {
                        lastIndex++;
                        continue;
                    }
                    if (!item.field_imagen_principal_path) {
                        lastIndex++;
                        continue;
                    }
                } catch(e){
                    continue;
                }
                item.field_imagen_principal_path = item.field_imagen_principal_path.split(' ').join('%20');
                if(item.field_kml_path) {
                    responses.push('<p><b>' + item.title + '.</b></p><p>' + item.descripcion + '</p><p>Track de la ruta: ' + item.field_kml_path + '.</p>' + item.field_imagen_principal_path)
                } else {
                    responses.push('<p><b>' + item.title + '.</b></p><p>' + item.descripcion + '</p>' + item.field_imagen_principal_path)
                }
            }
            userStorage.set('AstibotModule_i', lastIndex);
            if(data.length > lastIndex) {
                responses.push(`He encontrado ${data.length-lastIndex} resultados más. Si quieres puedo mostrarte más.`);
            }
            if (responses.length > 1) {
                utils.reply(bot, message, {
                    text: responses,
                    type: 'multiple'
                });
            } else {
                userStorage.remove('AstibotModule');
                userStorage.remove('AstibotModule_intent');
                utils.reply(bot, message, {
                    text: "No hay datos de rutas para la población indicada",
                    type: 'simple'
                });
            }
        } else {
            utils.reply(bot, message, {
                text: "No hay datos de rutas para la población indicada",
                type: 'simple'
            });
        }

    } else {
        utils.reply(bot, message, {
            text: "Lo siento, pero para poder mostrarte rutas necesito saber el municipio del que desea la información. Por favor, añada el municipio del que desea la información a su consulta.",
            type: 'multiple'
        });
    }
}



const _isValidJSON = (str) => {
    try {
        JSON.parse(str);
    } catch (e) {
        logger.error('JSON is not valid');
        return false;
    }
    return true;
};

module.exports = (options, imports, register) => {
    register(null, {
        astibotModule: {
            links: require('./links.json'),
            isGoingToRain,
            getTiempo,
            getHora,
            getFecha,
            getMonumento,
            getCoordenadas,
            getEmpresas,
            getEmpresasMunicipio,
            // getNoticias,
            // getNoticiasMunicipio,
            getOfertas,
            getOfertasMunicipio,
            getEventos,
            getEventosMunicipio,
            getRutas
        }
    });
};


// const getAllNoticias = () => {
//     return new Promise(function (resolve, reject) {
//         request.get({ url: 'https://turismoapps.badajoz.es/api/activos?tipo=noticia', strictSSL: false }, function (error, response, body) {
//             if (error) return reject(error);
//             if (!_isValidJSON(body)) return reject(error);
//             resolve(JSON.parse(body));
//         });
//     });
// };

// const getNoticiasByNid = (userStorage) => {
//     return new Promise(function (resolve, reject) {
//         getAllNoticias().then(async (result) => {
//             let noticias = [];
//             for (let item of result) {
//                 let noticia = await getNoticiaByNid(item.nid);
//                 if (noticia != null) noticias.push(noticia);
//             }
//             userStorage.set('AstibotModule', noticias);
//             userStorage.remove('AstibotModule_intent');
//             userStorage.set('AstibotModule_intent', 'getNoticiasByNid');
//             resolve(noticias);
//         });
//     });
// };

// const getNoticiaByNid = async (nid) => {
//     return new Promise(function (resolve, reject) {
//         request.get({ url: 'http://turismoapps.badajoz.es/api/noticia?nid=' + nid, strictSSL: false }, function (error, response, body) {
//             if (error) {
//                 resolve(null);
//                 return;
//             }
//             if (!_isValidJSON(body)) {
//                 resolve(null);
//                 return;
//             }
//             let data = JSON.parse(body);
//             resolve(data[0]);
//         });
//     });
// };

// const getNoticiasFromMunicipio = (nameMunicipality, userStorage) => {
//     return getAllMunicipios().then(() => {
//         return new Promise(function (resolve, reject) {
//             const mun = (municipios) ? municipios.find(obj => obj.title === nameMunicipality) : null;
//             if (!mun) return resolve();
//             request.get({ url: 'https://turismoapps.badajoz.es/api/noticia?id_municipio=' + mun.nid, strictSSL: false }, function (error, response, body) {
//                 if (error) return reject(error);
//                 let data = null;
//                 if (_isValidJSON(body)) {
//                     data = JSON.parse(body);
//                 }
//                 userStorage.set('AstibotModule', data);
//                 userStorage.remove('AstibotModule_intent');
//                 userStorage.set('AstibotModule_intent', 'getNoticiasFromMunicipio');
//                 resolve(data);
//             });
//         });
//     });
// };

// const getNoticiasMunicipio = (message, resp, bot) => {

//     let municipio = (Array.isArray(resp.result.parameters['poblacion']) ? resp.result.parameters['poblacion'][0] : resp.result.parameters['poblacion']);

//     const userStorage = new UserStorage(message.channel, {persist:true});

//     if(municipio) {
//         getNoticiasFromMunicipio(municipio, userStorage).then((data) => {

//             if (data) {
//                 let responses = ['¡Aquí tienes!'];

//                 let firstIndex = 0;
//                 let lastIndex = routesNum;

//                 for (let i = firstIndex; i < lastIndex; i++) {
//                     let item = data[i];
//                     try{
//                         if (!item.title) {
//                             lastIndex++;
//                             continue;
//                         }
//                     } catch(e){
//                         continue;
//                     }
//                     text = '';
//                     text += '<p><b>' + item.title + '.</b></p>';
//                     if (item.descripcion) text += '<p>' + item.descripcion + '.</p>';
//                     if (item.field_imagen_principal_path){
//                         item.field_imagen_principal_path = item.field_imagen_principal_path.split(' ').join('%20');
//                         text += item.field_imagen_principal_path;
//                     } 
//                     responses.push(text);
//                 }
//                 userStorage.set('AstibotModule_i', lastIndex);
//                 if(data.length > lastIndex) {
//                     responses.push(`He encontrado ${data.length-lastIndex} resultados más. Si quieres puedo mostrarte más.`);
//                 } 
//                 if (responses.length > 1) {
//                     utils.reply(bot, message, {
//                         text: responses,
//                         type: 'multiple'
//                     });
//                 } else {
//                     userStorage.remove('AstibotModule');
//                     userStorage.remove('AstibotModule_intent');
//                     utils.reply(bot, message, {
//                         text: "No hay noticias para la población indicada",
//                         type: 'simple'
//                     });
//                 }
//             } else {
//                 utils.reply(bot, message, {
//                     text: "No se han encontrado noticias para la población indicada",
//                     type: 'simple'
//                 });
//             }
//         }).catch(err => {
//             logger.error(err)
//         });
//     }else {
//         let data = userStorage.get('AstibotModule');
//         let firstIndex = userStorage.get('AstibotModule_i');
//         let lastIndex = firstIndex + routesNum;
        
//         if (data) {
//             let responses = ['¡Aquí tienes!'];

//             for (let i = firstIndex; i < lastIndex; i++) {
//                 let item = data[i];
//                 try{
//                     if (!item.title) {
//                         lastIndex++;
//                         continue;
//                     }
//                 } catch(e){
//                     continue;
//                 }
//                 text = '';
//                 text += '<p><b>' + item.title + '.</b></p>';
//                 if (item.descripcion) text += '<p>' + item.descripcion + '.</p>';
//                 if (item.field_imagen_principal_path){
//                     item.field_imagen_principal_path = item.field_imagen_principal_path.split(' ').join('%20');
//                     text += item.field_imagen_principal_path;
//                 }
//                 responses.push(text);
//             }
//             userStorage.set('AstibotModule_i', lastIndex);
//             if(data.length > lastIndex) {
//                 responses.push(`He encontrado ${data.length-lastIndex} resultados más. Si quieres puedo mostrarte más.`);
//             } 
//             if (responses.length > 1) {
//                 utils.reply(bot, message, {
//                     text: responses,
//                     type: 'multiple'
//                 });
//             } else {
//                 userStorage.remove('AstibotModule');
//                 userStorage.remove('AstibotModule_intent');
//                 utils.reply(bot, message, {
//                     text: "No hay noticias para la población indicada",
//                     type: 'simple'
//                 });
//             }
//         } else {
//             utils.reply(bot, message, {
//                 text: "No se han encontrado noticias para la población indicada",
//                 type: 'simple'
//             });
//         }
//     }
// }

// const getNoticias = (message, resp, bot) => {

//     const userStorage = new UserStorage(message.channel, {persist:true});
    
//     if(userStorage.get('AstibotModule_intent') == 'getNoticiasByNid'){
//         let data = userStorage.get('AstibotModule');
//         let firstIndex = userStorage.get('AstibotModule_i');
//         let lastIndex = firstIndex + routesNum;
        
//         if (data) {
//             let responses = ['¡Aquí tienes!'];

//             for (let i = firstIndex; i < lastIndex; i++) {
//                 let item = data[i];
//                 try{
//                     if (!item.title) {
//                         lastIndex++;
//                         continue;
//                     }
//                 } catch(e){
//                     continue;
//                 }
//                 text = '';
//                 text += '<p><b>' + item.title + '.</b></p>';
//                 if (item.descripcion) text += '<p>' + item.descripcion + '.</p>';
//                 if (item.field_imagen_principal_path){
//                     item.field_imagen_principal_path = item.field_imagen_principal_path.split(' ').join('%20');
//                     text += item.field_imagen_principal_path;
//                 }
//                 responses.push(text);
//             }
//             userStorage.set('AstibotModule_i', lastIndex);
//             if(data.length > lastIndex) {
//                 responses.push(`He encontrado ${data.length-lastIndex} resultados más. Si quieres puedo mostrarte más.`);
//             } 
//             if (responses.length > 1) {
//                 utils.reply(bot, message, {
//                     text: responses,
//                     type: 'multiple'
//                 });
//             } else {
//                 userStorage.remove('AstibotModule');
//                 userStorage.remove('AstibotModule_intent');
//                 utils.reply(bot, message, {
//                     text: "No se han encontrado noticias",
//                     type: 'simple'
//                 });
//             }
//         } else {
//             utils.reply(bot, message, {
//                 text: "No se han encontrado noticias",
//                 type: 'simple'
//             });
//         }
//     }else {
//         getNoticiasByNid(userStorage).then((data) => {

//             if (data) {
//                 let responses = ['¡Aquí tienes!'];

//                 let firstIndex = 0;
//                 let lastIndex = routesNum;

//                 for (let i = firstIndex; i < lastIndex; i++) {
//                     let item = data[i];
//                     try{
//                         if (!item.title) {
//                             lastIndex++;
//                             continue;
//                         }
//                     } catch(e){
//                         continue;
//                     }
//                     text = '';
//                     text += '<p><b>' + item.title + '.</b></p>';
//                     if (item.descripcion) text += '<p>' + item.descripcion + '.</p>';
//                     if (item.field_imagen_principal_path){
//                         item.field_imagen_principal_path = item.field_imagen_principal_path.split(' ').join('%20');
//                         text += item.field_imagen_principal_path;
//                     }
//                     responses.push(text);
//                 }
//                 userStorage.set('AstibotModule_i', lastIndex);
//                 if(data.length > lastIndex) {
//                     responses.push(`He encontrado ${data.length-lastIndex} resultados más. Si quieres puedo mostrarte más.`);
//                 }
//                 if (responses.length > 1) {
//                     utils.reply(bot, message, {
//                         text: responses,
//                         type: 'multiple'
//                     });
//                 } else {
//                     userStorage.remove('AstibotModule');
//                     userStorage.remove('AstibotModule_intent');
//                     utils.reply(bot, message, {
//                         text: "No se han encontrado noticias",
//                         type: 'simple'
//                     });
//                 }
//             } else {
//                 utils.reply(bot, message, {
//                     text: "No se han encontrado noticias",
//                     type: 'simple'
//                 });
//             }
//         }).catch(err => {
//             logger.error(err)
//         });
        
//     }
// }